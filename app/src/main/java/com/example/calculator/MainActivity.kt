package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private var firstVariable:Double=0.0


    private var secondVariable:Double=0.0


    private var operation = ""

    private fun init(){
        Button0.setOnClickListener(this)
        Button1.setOnClickListener(this)
        Button2.setOnClickListener(this)
        Button3.setOnClickListener(this)
        Button4.setOnClickListener(this)
        Button5.setOnClickListener(this)
        Button6.setOnClickListener(this)
        Button7.setOnClickListener(this)
        Button8.setOnClickListener(this)
        Button9.setOnClickListener(this)
        DeleteButton.setOnLongClickListener() {
            ResultTextView.text = ""
            operation.isEmpty()
            true }

        DotButton.setOnClickListener {
            if(ResultTextView.text.isNotEmpty() && ("." !in ResultTextView.text.toString()))
                ResultTextView.text = ResultTextView.text.toString() + "." }
    }
    fun delete(view: View){
        val value: String = ResultTextView.text.toString()
        if (value.length > 0){
            ResultTextView.text = value.substring(0, value.length - 1)
        }


    }

    fun divide(view: View){
        var value = ResultTextView.text.toString()
        if (value.isNotEmpty())
            operation = ":"
        firstVariable = value.toDouble()
        ResultTextView.text = ""
    }

    fun equals(view: View) {
        var value = ResultTextView.text.toString()
        if (value.isNotEmpty()) {
            secondVariable = value.toDouble()
            var result: Double = 0.0
            if (secondVariable.toDouble() == 0.0) {
                Toast.makeText(this, "Cannot Divide By Zero", Toast.LENGTH_SHORT).show()
            }
            if (operation == ":") {
                result = firstVariable / secondVariable
            } else if (operation == "x") {
                result = firstVariable * secondVariable
            } else if (operation == "+") {
                result = firstVariable + secondVariable
            } else if (operation == "-") {
                result = firstVariable - secondVariable
            }

            ResultTextView.text = result.toString()
        }
    }

    fun multiply(view: View){
        var value = ResultTextView.text.toString()
        if (value.isNotEmpty())
            operation = "x"
        firstVariable = value.toDouble()
        ResultTextView.text = ""
    }
    fun plus(view: View){
        var value = ResultTextView.text.toString()
        if (value.isNotEmpty())
            operation = "+"
        firstVariable = value.toDouble()
        ResultTextView.text = ""
    }
    fun minus(view: View){
        var value = ResultTextView.text.toString()
        if (value.isNotEmpty())
        operation = "-"
        firstVariable = value.toDouble()
        ResultTextView.text = ""
    }
    fun dot(view: View){
        ResultTextView.text = ResultTextView.text.toString() + "."
        DotButton.isClickable == false

    }


    override fun onClick(v: View?) {
        val button = v as Button
        ResultTextView.text = ResultTextView.text.toString() + button.text.toString()
    }
}